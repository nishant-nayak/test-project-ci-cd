terraform {
  backend "http" {}

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~>3.7.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.75.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~>2.4.1"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_access_token
}

provider "azurerm" {
  features {}
}

provider "kubernetes" {
  host                   = azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.host
  username               = azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.username
  password               = azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.password
  client_certificate     = base64decode(azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.django_deployment_cluster.kube_config.0.cluster_ca_certificate)
}

# Resource Group to house all Azure Resources
resource "azurerm_resource_group" "test_rg" {
  name     = var.azure_rg_name
  location = var.azure_rg_location
}
