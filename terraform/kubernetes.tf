# Create the K8s deployment
resource "kubernetes_deployment" "django_k8s_deployment" {
  metadata {
    name = "django-deployment"
    labels = {
      "app" = "auctions"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = "auctions"
      }
    }

    template {
      metadata {
        labels = {
          "app" = "auctions"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/nishant-nayak/test-project-ci-cd:django"
          name  = "auctions-container"
          port {
            container_port = 80
          }

          env {
            name  = "DJANGO_SECRET_KEY"
            value = var.django_secret_key
          }
          env {
            name  = "MYSQL_DB_HOST"
            value = azurerm_mysql_server.test_mysql_server.fqdn
          }
          env {
            name  = "MYSQL_DB_USER"
            value = "${var.mysql_username}@${azurerm_mysql_server.test_mysql_server.name}"
          }
          env {
            name  = "MYSQL_DB_PASSWORD"
            value = var.mysql_password
          }
          env {
            name  = "MYSQL_DB_NAME"
            value = var.mysql_db_name
          }
        }

      }
    }
  }
}

# Create the load balancer
resource "kubernetes_service" "django_load_balancer" {
  metadata {
    name = "django-load-balancer"
  }
  spec {
    selector = {
      "app" = kubernetes_deployment.django_k8s_deployment.metadata.0.labels.app
    }
    port {
      port = 8000
    }
    type = "LoadBalancer"
  }
}
