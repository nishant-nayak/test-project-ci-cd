# Creates a Kubernetes Cluster in Azure
resource "azurerm_kubernetes_cluster" "django_deployment_cluster" {
  name                = var.aks_cluster_name
  location            = azurerm_resource_group.test_rg.location
  resource_group_name = azurerm_resource_group.test_rg.name
  dns_prefix          = "djangoaks1"

  default_node_pool {
    name                = "default"
    vm_size             = "Standard_DS2_v2"
    enable_auto_scaling = false
    # min_count = 1
    # max_count = 32
    node_count = 1
  }

  identity {
    type = "SystemAssigned"
  }

  lifecycle {
    ignore_changes = [
      default_node_pool["node_count"]
    ]
  }
}
