FROM mcr.microsoft.com/azure-cli:latest
RUN apk add --no-cache jq curl git openssh
RUN az aks install-cli --out none

WORKDIR /tmp

RUN ( curl -sLo terraform.zip "https://releases.hashicorp.com/terraform/1.1.4/terraform_1.1.4_linux_amd64.zip" && \
      unzip terraform.zip && \
      rm terraform.zip && \
      mv ./terraform /usr/local/bin/terraform \
    ) && terraform --version

WORKDIR /

COPY gitlab-terraform.sh /usr/bin/gitlab-terraform
RUN chmod +x /usr/bin/gitlab-terraform
